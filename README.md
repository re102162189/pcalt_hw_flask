## Flask with CRUD preparation

### install MySQL using docker
```
$ docker run -d \
  --name=mysql \
  -e="MYSQL_ROOT_PASSWORD=mypwd123456" \
  -p 3306:3306 \
  mysql:5.7
```

### install dependencies
```
$ pip3 install -r requirements.txt
```

### init MySQL database and table
```
$ python3 db_scripts/db_init.py
```

### start Flask
```
$ python3 user_controller.py
```

### perform CRUD via command-line curl
```
# create a new user using POST
$ curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"name": "kite", "email": "re102162189@gmail.com"}' \
  http://localhost:5001/users

# update a existing user using PATCH
$ curl --header "Content-Type: application/json" \
  --request PATCH \
  --data '{"email": "kite.chen@gmail.com"}' \
  http://localhost:5001/users/1/email  

# search a non-existing user using PATCH
$ curl --header "Content-Type: application/json" \
  --request PATCH \
  --data '{"email": "kite.chen@gmail.com"}' \
  http://localhost:5001/users/2/email    

# search a existing user using GET
$ curl --request GET \
  http://localhost:5001/users/kite     

# delete a existing user using DELETE
$ curl --request DELETE \
  http://localhost:5001/users/1       
```

### stop Flask and remove database
```
# ctrl + C to stop Flask
$ python3 db_scripts/db_destory.py
```