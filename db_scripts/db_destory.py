import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="mypwd123456",
  database="demo"
)

mysqlcursor = mydb.cursor()

sql = "DROP TABLE IF EXISTS users"
mysqlcursor.execute(sql)
print("drop TABLE users")

sql = "DROP DATABASE IF EXISTS demo"
mysqlcursor.execute(sql)
print("drop DATABASE demo")
mydb.close()