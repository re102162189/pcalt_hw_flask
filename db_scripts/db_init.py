import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="mypwd123456"
)

mysqlcursor = mydb.cursor()
mysqlcursor.execute("CREATE DATABASE demo")
print('DATABASE demo created')
mydb.close()

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="mypwd123456",
  database="demo"
)

mysqlcursor = mydb.cursor()
mysqlcursor.execute('''
  CREATE TABLE users (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL, 
    email VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
  )
''')
print('TABLE users created')
mydb.close()