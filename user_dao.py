import mysql.connector
from user import User

class UserDao():
    def __init__(self, host:str, user:str, 
                        password:str, database:str):
        self.mydb = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )

    def create(self, user:User):
        sql = "INSERT INTO users (name, email) VALUES (%s, %s)"
        with self.mydb.cursor(prepared=True) as curs:
            curs.execute(sql, (user.get_name(), user.get_email()))
            self.mydb.commit()
        return True

    def find_by_name(self, name:str):
        sql = "SELECT * FROM users WHERE name = %s"
        with self.mydb.cursor(prepared=True) as curs:
            curs.execute(sql, (name,))
            result = curs.fetchone()
        return result

    def find_by_id(self, id:int):
        sql = "SELECT * FROM users WHERE id = %s"
        with self.mydb.cursor(prepared=True) as curs:
            curs.execute(sql, (id,))
            result = curs.fetchone()
        return result    

    def update_email(self, id:int, email:str):
        sql = "UPDATE users SET email = %s WHERE id = %s"
        with self.mydb.cursor(prepared=True) as curs:
            curs.execute(sql, (email, id))
            self.mydb.commit()
        return True

    def delete(self, id:int):
        sql = "DELETE FROM users WHERE id = %s"
        with self.mydb.cursor(prepared=True) as curs:
            curs.execute(sql, (id,))
            self.mydb.commit()
        return True