from flask import Flask, jsonify, request
from user_dao import UserDao
from user import User

URL_PREFIX = '/users'
app = Flask(__name__)
dao = UserDao(host='localhost', user='root', 
              password='mypwd123456', database='demo')

@app.route(URL_PREFIX + '/<string:name>', methods=['GET'])
def find(name:str):
    user = dao.find_by_name(name)
    if(user is None):
        return jsonify({'error': 'no such user'}), 200
    else:    
        return jsonify({'user': user}), 200

@app.route(URL_PREFIX, methods=['POST'])
def create():
    json_data = request.json
    name = json_data['name']
    email = json_data['email']
    if(name is None or email is None):
        return jsonify({'error': 'name/email should not be empty'}), 403

    try:
        user = User(name, email)
        dao.create(user)
    except: 
        return jsonify({"error": "db exception - create"}), 500

    return jsonify({'result': 'created'}), 200

@app.route(URL_PREFIX + '/<int:id>/email', methods=['PATCH'])
def update(id:int):
    json_data = request.json
    email = json_data['email']
    if(email is None):
        return jsonify({'error': 'email should not be empty'}), 403
    
    try:
        user = dao.find_by_id(id)
        if(user is None):
            return jsonify({'error': 'no such user'}), 200
        dao.update_email(id, email)    
    except: 
        return jsonify({"error": "db exception"}), 500

    return jsonify({'result': 'updated'}), 200

@app.route(URL_PREFIX + '/<int:id>', methods=['DELETE'])
def delete(id:int):
    try:
        user = dao.find_by_id(id)
        if(user is None):
            return jsonify({'error': 'no such user'}), 200
        dao.delete(id)
    except: 
        return jsonify({"error": "db exception - delete"}), 500  

    return jsonify({'result': 'deleted'}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)